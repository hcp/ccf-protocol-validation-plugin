package org.nrg.ccf.protocolval.xapi;

import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.ccf.protocolval.entities.ProtocolValConfig;
import org.nrg.ccf.protocolval.services.ProtocolValConfigService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Protocol Validation Configuration API")
public class ProtocolValidationConfigApi extends AbstractXapiRestController {
	
	public static Logger _logger = Logger.getLogger(ProtocolValidationConfigApi.class);
	private ProtocolValConfigService _configService;

	@Autowired
	protected ProtocolValidationConfigApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, ProtocolValConfigService configService) {
		super(userManagementService, roleHolder);
		_configService = configService;
	}
	
	@ApiOperation(value = "Sets Protocol Validation Configuration", notes = "Sets protocol validation configuration.",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/protocolValidationConfig"},
    						restrictTo=AccessLevel.Owner, consumes= {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> removeProtocolValidation(@PathVariable("projectId") @ProjectId final String projectId,
    												@RequestBody final ProtocolValConfig config) {
		if (!projectId.equals(config.getProjectId())) {
			return new ResponseEntity<>("ERROR:  The projectId in the configuration doesn't match the projectId in the URL.",HttpStatus.UNPROCESSABLE_ENTITY);
		}
		_configService.removeConfig(config);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Sets Protocol Validation Configuration", notes = "Sets protocol validation configuration.",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/protocolValidationConfig"},
    						restrictTo=AccessLevel.Owner, consumes= {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> setProtocolValidation(@PathVariable("projectId") @ProjectId final String projectId,
    												@RequestBody final ProtocolValConfig config) {
		if (!projectId.equals(config.getProjectId())) {
			return new ResponseEntity<>("ERROR:  The projectId in the configuration doesn't match the projectId in the URL.",HttpStatus.UNPROCESSABLE_ENTITY);
		}
		final String content = config.getSchematronContent();
		final String desc = config.getSchematronDescription();
		//final String notifyType = config.getNotificationType();
		if (content==null || content.length()<1 || desc==null || desc.length()<1) {
			return new ResponseEntity<>("ERROR:  A schematron and schematron description must be provided", HttpStatus.UNPROCESSABLE_ENTITY);
		}
		_configService.saveOrUpdate(config);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Gets Protocol Validation Configuration", notes = "Gets protocol validation configuration.",
			response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/protocolValidationConfig"},
    						restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<ProtocolValConfig>> getProtocolValidationConfig(@PathVariable("projectId") @ProjectId final String projectId,
    		@RequestParam(value="condensed", defaultValue="false") final Boolean condensed) {
		List<ProtocolValConfig> entities = _configService.getConfigEntities(projectId);
		if (condensed) {
			for (ProtocolValConfig entity : entities) {
				entity.setSchematronContent(null);
				entity.setCreated(null);
				entity.setDisabled(null);
				entity.setNotificationList(null);
			}
		}
		return new ResponseEntity<>(entities, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Gets Protocol Validation Configuration", notes = "Gets protocol validation configuration.",
			response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/xsiType/{xsiType}/protocolValidationConfig"},
    						restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ProtocolValConfig> runProtocolValidationConfig(
    		@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("xsiType") final String xsiType) {
		final ProtocolValConfig config =_configService.getConfigEntity(projectId, xsiType);
		if (config != null) {
			return new ResponseEntity<>(config , HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}

package org.nrg.ccf.protocolval.xapi;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.ccf.protocolval.components.Validator;
import org.nrg.ccf.protocolval.constants.NotificationType;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Protocol Validation API")
public class ProtocolValidationApi extends AbstractXapiRestController {
	
	public static Logger _logger = Logger.getLogger(ProtocolValidationApi.class);
	private Validator _validator;

	@Autowired
	protected ProtocolValidationApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, Validator validator) {
		super(userManagementService, roleHolder);
		_validator = validator;
	}
	
	@ApiOperation(value = "Get image session types", notes = "Get image session types.",
			response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/getImageSessionTypes"},
    						restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getImageSessionTypes(@PathVariable("projectId") @ProjectId final String projectId) {
		List<String> returnList = new ArrayList<>();
		//returnList.add(XnatMrsessiondata.SCHEMA_ELEMENT_NAME);
		List<XnatImagesessiondata> sessionList = 
				XnatImagesessiondata.getXnatImagesessiondatasByField("xnat:imageSessionData/project", projectId, getSessionUser(), false);
		for (XnatImagesessiondata session : sessionList) {
			if (!returnList.contains(session.getXSIType())) {
				returnList.add(session.getXSIType());
			}
		}
		return new ResponseEntity<>(returnList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Runs protocol validation", notes = "Runs protocol validation.",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/projects/{projectId}/subjects/{subjectId}/experiments/{experimentId}/runProtocolValidation"},
    						restrictTo=AccessLevel.Edit, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> runProtocolValidation(@PathVariable("projectId") @ProjectId final String projectId,
    												  @PathVariable("subjectId") final String subjectId,
    												  @PathVariable("experimentId") final String experimentId,
    												  @RequestParam(value = "notification", required = false) final NotificationType notification) {
		String rtn;
		PersistentWorkflowI wrk = null;
		EventMetaI ci = null;
		try {
			final UserI user = getSessionUser();
			final XnatExperimentdataI exp = _validator.getExperiment(projectId, experimentId, user);
			final XnatExperimentdata expData = (XnatExperimentdata)exp;
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getSessionUser(), expData.getItem(), 
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, "Ran ProtocolValidation", null, null));
			ci = wrk.buildEvent();
			rtn = _validator.validate(projectId, exp, getSessionUser(), notification);
			ResponseEntity<String> entity = new ResponseEntity<>(rtn,HttpStatus.OK);
			PersistentWorkflowUtils.complete(wrk, ci);
			return entity;
		} catch (Exception e) {
			rtn = "Protocol Validation Threw an Exception: " + e.toString();
			try {
				if (wrk != null && ci != null) {
					PersistentWorkflowUtils.fail(wrk, ci);
				}
			} catch (Exception e2) {
				_logger.error("Exception saving workflow: ", e2);
			}
			return new ResponseEntity<>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}

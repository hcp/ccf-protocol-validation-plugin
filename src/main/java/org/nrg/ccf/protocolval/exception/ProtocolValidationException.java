package org.nrg.ccf.protocolval.exception;

public class ProtocolValidationException extends Exception {

	private static final long serialVersionUID = -4554877184331178573L;

	public ProtocolValidationException() {
		super();
	}

	public ProtocolValidationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ProtocolValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProtocolValidationException(String message) {
		super(message);
	}

	public ProtocolValidationException(Throwable cause) {
		super(cause);
	}

}

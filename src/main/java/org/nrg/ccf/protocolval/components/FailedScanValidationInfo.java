package org.nrg.ccf.protocolval.components;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FailedScanValidationInfo extends ValidationInfo {
	
	private String numScanId;
	private String scanId;
	private String seriesDescription;
	private String scanType;
	private String scanQuality;
	private String scanStatus;
	private String checkId;
	private String status;
	private String xmlPath;
	private String verified;
	private String diagnosis;
	
}

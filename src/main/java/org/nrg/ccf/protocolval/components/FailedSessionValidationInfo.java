package org.nrg.ccf.protocolval.components;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FailedSessionValidationInfo extends ValidationInfo {
	
	private String status;
	private String diagnosis;
	private String checkId;
	
}

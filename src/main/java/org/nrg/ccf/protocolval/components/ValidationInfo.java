package org.nrg.ccf.protocolval.components;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationInfo {
	
	private String id;
	private String project;
	private String label;
	private String imageSessionId;
	private String imageSessionLabel;
	private Date imageSessionDate;
	private String imageSessionScanner;
	private String imageSessionOperator;
	private String imageSessionSite;
	private String overallStatus;
	
}

package org.nrg.ccf.protocolval.components;

import net.sf.saxon.TransformerFactoryImpl;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.nrg.action.ClientException;
import org.nrg.ccf.protocolval.constants.NotificationType;
import org.nrg.ccf.protocolval.entities.ProtocolValConfig;
import org.nrg.ccf.protocolval.services.ProtocolValConfigService;
import org.nrg.ccf.protocolval.util.PluginUriResolver;
import org.nrg.ccf.protocolval.utils.ValidateUtils;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.mail.services.MailService;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.ValProtocoldata;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.uri.UriParserUtils;
import org.nrg.xnat.services.archive.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

@Component
public class Validator {
	
	private final static TransformerFactory _transformerFactory = 
			TransformerFactoryImpl.newInstance( "net.sf.saxon.TransformerFactoryImpl", null);
	private final static DocumentBuilderFactory _builderFactory = 
			DocumentBuilderFactory.newInstance( "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl", null);
	private static DocumentBuilder _builder;
	private static URIResolver _uriResolver;
	private final static Logger _logger = Logger.getLogger(Validator.class); 
	private final ProtocolValConfigService _configService;
	private final CatalogService _catalogService;
	
	@Autowired
	public Validator(ProtocolValConfigService configService, CatalogService catalogService) {
		super();
		this._configService = configService;
		this._catalogService = catalogService;
	}

	static {
		_builderFactory.setNamespaceAware(true);
		try {
			_builder = _builderFactory.newDocumentBuilder();
			_uriResolver = new PluginUriResolver(_builder);
			_transformerFactory.setURIResolver(_uriResolver);
		} catch (ParserConfigurationException e) {
			_logger.error("ERROR:  Could not initialize DocumentBuilder");
		}
	}
	
	public XnatExperimentdataI getExperiment(String projId, String expId, UserI user) throws Exception {
		
		XnatExperimentdataI exp = XnatExperimentdata.getXnatExperimentdatasById(expId, user, true);
		if (exp == null) { 

	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	    	
	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	        subcc1.addClause("xnat:experimentData/project", projId);
	        subcc1.addClause("xnat:experimentData/label", expId);
	        cc.add(subcc1);
	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	    	subcc2.addClause("xnat:experimentData/sharing/share/project", projId);
	    	subcc2.addClause("xnat:experimentData/sharing/share/label", expId);
	        cc.add(subcc2);

			final List<XnatExperimentdata> expList = XnatExperimentdata.getXnatExperimentdatasByField(cc, user, true);
			if (!expList.isEmpty()) {
				exp = expList.get(0);
			}
		}
		return exp;
		
	}
	
	public String validate(String projId, String expId, UserI user, NotificationType notification) throws Exception {
		
		return validate(projId, getExperiment(projId, expId, user), user, notification);
		
	}
	
	public String validate(String projId, XnatExperimentdataI exp, UserI user, NotificationType notify) throws Exception {
		
		if (!Permissions.canEditProject(user, projId)) {
			throw new IllegalAccessException("You must have edit access to run protocol validation");
		}
		if (exp == null) {
			throw new ClientException("Experiment requested is null");
		}
		
		String buildDir = PipelineFileUtils.getBuildDir(exp.getProject(), true);
    	final Date date = new Date();
    	final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	    final String dateSuffix = formatter.format(date);
		buildDir +=   "archive_trigger" + File.separator +  dateSuffix + File.separator + exp.getLabel() +
			File.separator + "VALIDATION";
		final File buildDirF = new File(buildDir);
		FileUtils.forceMkdir(buildDirF);
		final ProtocolValConfig configEntity = _configService.getConfigEntity(projId, exp.getXSIType());
		final StringBuilder logBuilder = new StringBuilder();
		
		logBuilder.append("Begin protocol validation processing (EXP=" + exp.getLabel() + ")");
		logBuilder.append(System.lineSeparator());
		
		ValProtocoldata pData = null;
		String archiveUri = null; 
		
		boolean notified = false;
		try {
		
	
			final StringWriter writer = new StringWriter();
			exp.toXML(writer);
			final String expXml = writer.toString();
			writer.close();
			final File expF = new File(buildDirF, exp.getLabel() + ".xml");
			FileUtils.writeStringToFile(expF, expXml);
			
			try {
				
				//
				// STEP 1
				//
				
				logBuilder.append("STEP 1:  Get protocol validation configuration (PROJ=" + projId + ",EXP=" + exp.getLabel() + ").");
				if (configEntity==null) {
					throw new ClientException("Protocol validation is not configured in this project for this session type (" +
							exp.getXSIType() + ").");
				}
				final String schematron = configEntity.getSchematronContent();
				if (schematron==null) {
					throw new ClientException("Configuration Error.  Could not obtain schematron from configuration");
				}
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 2
				//
				
				logBuilder.append("STEP 2:  Get plugin resources");
				final Resource svrlR = BasicXnatResourceLocator.getResource("/META-INF/protocolVal/svrl/nrg_iso_svrl_for_xslt2.xsl");
				final Resource validateR = BasicXnatResourceLocator.getResource("/META-INF/protocolVal/create_validate.xsl");
				final Resource attachmentR = BasicXnatResourceLocator.getResource("/META-INF/protocolVal/create_text_attachment.xsl");
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 3
				//
				
				logBuilder.append("STEP 3:  Transform schematron with SVRL resource to create Rules XSL");
				final String rulesXsl = transform(stringToSrc(schematron), resourceToSrc(svrlR));
				final File rulesXslF = new File(buildDirF, exp.getLabel() + "_rule.xsl");
				FileUtils.writeStringToFile(rulesXslF, rulesXsl);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 4
				//
				
				logBuilder.append("STEP 4:  Transform session XML with Rules XSL to generate Report XML");
				final Map<String,String> reportMap = new HashMap<>();
				reportMap.put("experimentfilename", expF.getCanonicalPath());
				final String reportXml = transform(stringToSrc(expXml), stringToSrc(rulesXsl), reportMap);
				final File reportXmlF = new File(buildDirF, exp.getLabel() + "_report.xml");
				FileUtils.writeStringToFile(reportXmlF, reportXml);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 5
				//
				
				logBuilder.append("STEP 5:  Transform report XML with validation XML to create assessor XML");
				final Map<String,String> validationMap = new HashMap<>();
				final File validationXmlF = new File(buildDirF, exp.getLabel() + "_validation.xml");
				validationMap.put("reportfilename", reportXmlF.getCanonicalPath());
				validationMap.put("rulefilename", configEntity.getSchematronDescription());
				validationMap.put("rulesxslfilename", rulesXslF.getCanonicalPath());
				validationMap.put("xslfilename", svrlR.getFilename());
				validationMap.put("experimentfilename", expF.getCanonicalPath());
				validationMap.put("validationfilename", validationXmlF.getCanonicalPath());
				validationMap.put("validationxslfilename", validateR.getFilename());
				validationMap.put("uri_content", "v0.1");
				final String validation_id = exp.getLabel() + "_PC_" + dateSuffix;
				validationMap.put("validation_id", validation_id);
				final String validationXml = transform(stringToSrc(reportXml), resourceToSrc(validateR), validationMap);
				FileUtils.writeStringToFile(validationXmlF, validationXml);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator());
				logBuilder.append(validationMap.toString());
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 6
				//
				
				logBuilder.append("STEP 6:  Removing existing validaiton assessors, if any.");
				logBuilder.append(System.lineSeparator());
				final CriteriaCollection valCC = new CriteriaCollection("AND");
	  	      	valCC.addClause("val:ProtocolData.imageSession_ID",exp.getId());
	  	      	valCC.addClause("val:ProtocolData.project",projId);
				List<ValProtocoldata> currDatas = ValProtocoldata.getValProtocoldatasByField(valCC, user, false); 
				if (currDatas.size()<1) {
					logBuilder.append(System.lineSeparator());
					logBuilder.append("   None found.");
					
				} else {
					for (final ValProtocoldata currVal : currDatas) {
						if (currVal.canDelete(user)) {
							logBuilder.append(System.lineSeparator());
							logBuilder.append("   Removing " + currVal.getLabel());
							PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user,  currVal.getItem(), EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE,
											EventUtils.DELETE_VIA_WEB_SERVICE));
							final EventMetaI ci = wrk.buildEvent();
							currVal.delete(currVal.getProjectData(), user, true, ci);
							PersistentWorkflowUtils.complete(wrk, ci);
						}
					}
				}
				logBuilder.append(System.lineSeparator());
				logBuilder.append("DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 7
				//
				
				logBuilder.append("STEP 7:  Store assessor XML (Create experiment assessor)");
				final String assessorId = ValidateUtils.storeAssessorXml(validationXml, validation_id, user);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 8
				//
				
				logBuilder.append("STEP 8:  Create attachment (email attachment)");
				final File attachmentTxtF = new File(buildDirF, exp.getLabel() + "_attachment.txt");
				final Map<String,String> attachmentMap = new HashMap<>();
				attachmentMap.put("xnatserver", getSiteId());
				attachmentMap.put("experimentfilename", expF.getCanonicalPath());
				final String attachmentTxt = transform(stringToSrc(reportXml), resourceToSrc(attachmentR), attachmentMap);
				//_logger.error("ATTACHMENT XML");
				//_logger.error(attachmentTxt);
				//System.out.println(validationXml);
				FileUtils.writeStringToFile(attachmentTxtF, attachmentTxt);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 9
				//
				
				logBuilder.append("STEP 9:  Create validation resource and upload files");
				pData = ValProtocoldata.getValProtocoldatasById(assessorId, user, false);
				archiveUri = UriParserUtils.getArchiveUri(exp, pData);
				File[] files = { expF, reportXmlF, rulesXslF, validationXmlF, attachmentTxtF };
				uploadFilesToResource(user, archiveUri, files);
				logBuilder.append(".....DONE");
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				logBuilder.append(System.lineSeparator() + System.lineSeparator());
				
				//
				// STEP 10
				//
				
				logBuilder.append("STEP 10:  Send notifications.");
				NotificationType notificationType = configEntity.getNotificationType();
				if (notify != null) {
						notificationType = notify;
				}
				final String notificationList = configEntity.getNotificationList();
				notified = true;
				if (notificationType != null && notificationList != null && !notificationType.equals(NotificationType.NONE) &&
						notificationList.contains("@")) {
					if (pData != null) {
						final String status = pData.getCheck_status();
						if ((notificationType.equals(NotificationType.ON_FAIL) &&
								status.toUpperCase().contains("FAIL")) ||
							 notificationType.equals(NotificationType.ALWAYS)) {
							_logger.error("SEND NOTIFICATION!!!!!!!!");
							sendNotification(exp, pData, user, projId, status, logBuilder,
									buildDirF, notificationList.split(","), attachmentTxtF);
						}
					}
				}
				return (pData != null) ? pData.getId() : null;
				
			} catch (FileNotFoundException ffe) { 
				
				_logger.error("ERROR:  One or more requested resources was not found.", ffe);
				throw ffe;
				
			} 
		
		} catch (Throwable t) {
			
			logBuilder.append(System.lineSeparator());
			logBuilder.append(ExceptionUtils.getFullStackTrace(t));
			throw t;
			
		} finally {
			
			final File validationLogF = new File(buildDirF, exp.getLabel() + "_log.txt");
			FileUtils.writeStringToFile(validationLogF, logBuilder.toString(), true);
			if (pData != null) {
				final File[] files = { validationLogF };
				uploadFilesToResource(user, archiveUri, files);
			}
			if (!notified) {
				final String notificationList = configEntity.getNotificationList();
				notified = true;
				sendNotification(exp, pData, user, projId, "PROCESSING_FAILURE", logBuilder,
									buildDirF, notificationList.split(","), null);
			}
			
		}
		
	}
	
	private void uploadFilesToResource(UserI user, String archiveUri, File[] files) throws Exception {
		if (archiveUri != null) {
			_catalogService.insertResources(user, archiveUri,
					Arrays.asList(files), "VALIDATION", "Validation Files", "MISC", "FILES", new String[] {});
		}
	}

	public String validate(String projId, String expId, UserI user, String notificationType) throws Exception {
		
		NotificationType notification = null;
		try {
			if (notificationType != null) {
				notification = NotificationType.valueOf(notificationType);
			}
		} catch (IllegalArgumentException e) {
			// Do nothing
		}
		
		return validate(projId, expId, user, notification);
		
	}
	
	public String validate(String projId, XnatExperimentdataI exp, UserI user, String notificationType) throws Exception {
		
		NotificationType notification = null;
		try {
			if (notificationType != null) {
				notification = NotificationType.valueOf(notificationType);
			}
		} catch (IllegalArgumentException e) {
			// Do nothing
		}
		
		return validate(projId, exp, user, notification);
		
	}
	

	private void sendNotification(XnatExperimentdataI exp, 
				ValProtocoldata pData, UserI user, String projId, String status, StringBuilder logBuilder,
				File buildDirF, String[] notificationArr, File attachmentTxtF) throws Exception {
			final MailService mailService = XDAT.getMailService();
			final String from = XDAT.getSiteConfigPreferences().getAdminEmail();
			final String siteId = XDAT.getSiteConfigPreferences().getSiteId();
			final String subject = siteId + " update: " + exp.getLabel() + " Validation " +
					((status.toUpperCase().contains("FAIL")) ? "Failed" + 
							((status.toUpperCase().contains("PROCESSING")) ? " (Processing Failure)" : "")
							: "Passed")
					;
		    final StringBuilder sb = new StringBuilder(); 
					sb.append("Dear " + user.getFirstname() + " " + user.getLastname() + ",<br><br>");
					final String site = exp.getAcquisitionSite();
					if (site != null && site.length()>0) {
						sb.append("Site:   " + exp.getAcquisitionSite() + "<br><br>");
					}
		    		if (exp instanceof XnatMrsessiondata) {
		    			final String scanner = ((XnatMrsessiondata)exp).getScanner();
		    			if (scanner != null && scanner.length()>0) {
		    				sb.append("Scanner:   " + ((XnatMrsessiondata)exp).getScanner() + "<br><br>");
		    			}
		    		}
					sb.append("A validation report for " + exp.getLabel() +
		   					" is now available and attached to this e-mail.<br><br>");
					
					if (pData != null) {
						sb.append("Details for this session are available at the ")
						.append("<a href=\"") 
						.append(TurbineUtils.GetFullServerPath()) 
						.append("/app/action/DisplayItemAction/search_element/val:protocolData/search_field/val:protocolData.ID/search_value/") 
						.append(pData.getId()) 
						.append("/project/") 	
						.append(projId) 
						.append("\">") 
						.append(siteId) 
						.append("</a></h3>\n")
						.append(" website." + "<br><br>");
					} else {
						sb.append("Processing terminated before generating a protocol assessor.<br><br>");
					}
		   			sb.append(siteId + " Team." + "<br><br>");
		   	final String htmlText = sb.toString();
			logBuilder.append(".....DONE");
		   	logBuilder.append(System.lineSeparator() + System.lineSeparator());
			logBuilder.append("Processing " + ((status.toUpperCase().contains("PROCESSING")) ? "Failed." : "Complete."));
		    final Map<String, File> attachments = new HashMap<>();
		    final File validationLogF = new File(buildDirF, exp.getLabel() + "_log.txt");
		    FileUtils.writeStringToFile(validationLogF, logBuilder.toString());
		    if (validationLogF != null && validationLogF.exists()) {
		    	attachments.put("Validate.log", validationLogF);
		    }
		    if (attachmentTxtF != null && attachmentTxtF.exists()) {
		    	attachments.put(attachmentTxtF.getName(), attachmentTxtF);
		    }
			mailService.sendHtmlMessage(from, notificationArr, null, null,
					subject, htmlText, null, attachments);
			// Remove file to clean up build directory.
			if (attachmentTxtF != null && attachmentTxtF.exists()) {
				attachmentTxtF.delete();
			}
	
	}

	private synchronized Source resourceToSrc(Resource res) throws IOException, SAXException, ParserConfigurationException {
		
		final InputStream stream = res.getInputStream();
		final Document doc = _builder.parse(stream);
		stream.close();
		return new DOMSource(doc);
		
	}

	private synchronized Source stringToSrc(String str) throws SAXException, IOException, ParserConfigurationException {
		
		final InputStream is = new ByteArrayInputStream(str.getBytes(Charset.defaultCharset()));
		final Document doc = _builder.parse(is);
		is.close();
		return new DOMSource(doc);
		
	}

	private synchronized String transform(Source xmlStream, Source xslStream, Map<String,String> parameterMap) throws TransformerException, IOException {
		
		final Templates template = _transformerFactory.newTemplates(xslStream);
		final Transformer transformer = template.newTransformer();
		for (String key : parameterMap.keySet()) {
			transformer.setParameter(key, parameterMap.get(key));
		}
		final StringWriter resultWriter = new StringWriter();
		final StreamResult result = new StreamResult(resultWriter);
		transformer.transform(xmlStream,result);
		final String resultStr = resultWriter.toString();
		resultWriter.close();
		return resultStr;
		
	}

	private synchronized String transform(Source stringToSrc, Source resourceToSrc) throws TransformerException, IOException {
		
		return transform(stringToSrc, resourceToSrc, new HashMap<String,String>());
		
	}
	
	private String getSiteId(){
		String site_id = XDAT.getSiteConfigPreferences().getSiteId();
		site_id = StringUtils.replace(site_id, " ", "");
		site_id = StringUtils.replace(site_id, "-", "_");
		site_id = StringUtils.replace(site_id, "\"", "");
		site_id = StringUtils.replace(site_id, "'", "");
		site_id = StringUtils.replace(site_id, "^", "");
		return site_id;
	}
	
}

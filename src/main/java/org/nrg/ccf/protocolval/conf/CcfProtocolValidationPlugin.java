package org.nrg.ccf.protocolval.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfProtocolValidationPlugin",
			name = "CCF Protocol Validation Plugin",
			entityPackages = "org.nrg.ccf.protocolval.entities",
			log4jPropertiesFile="/META-INF/protocolVal/protocolValLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.protocolval.conf",
		"org.nrg.ccf.protocolval.components",
		"org.nrg.ccf.protocolval.daos",
		"org.nrg.ccf.protocolval.services",
		"org.nrg.ccf.protocolval.xapi"
	})
public class CcfProtocolValidationPlugin {
	
	public static Logger logger = Logger.getLogger(CcfProtocolValidationPlugin.class);

	public CcfProtocolValidationPlugin() {
		logger.info("Configuring the CCF Protocol Validation Plugin.");
	}
	
}

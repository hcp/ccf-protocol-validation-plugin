
package org.nrg.ccf.protocolval.entities;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.nrg.automation.services.ScriptProperty;
import org.nrg.ccf.protocolval.constants.NotificationType;
import org.nrg.ccf.protocolval.util.ConfigProperty;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Properties;

@Audited
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"projectId", "xsiType"}))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class ProtocolValConfig extends AbstractHibernateEntity {

	private static final long serialVersionUID = -1479345585491441090L;
	private static final Logger _log = LoggerFactory.getLogger(ProtocolValConfig.class);
    
    private String _xsiType;
    private String _projectId;
    private String _schematronContent;
    private String _schematronDescription;
    private String _notificationList;
    private NotificationType _notificationType;

	public ProtocolValConfig() {
        if (_log.isDebugEnabled()) {
            _log.debug("Creating default Script object");
        }
    }

    public Properties toProperties() {
        final Properties properties = new Properties();
        properties.setProperty(ConfigProperty.XsiType.key(), _xsiType);
        properties.setProperty(ConfigProperty.ProjectId.key(), _projectId);
        properties.setProperty(ConfigProperty.SchematronContent.key(), _schematronContent);
        properties.setProperty(ConfigProperty.SchematronDescription.key(), _schematronDescription);
        properties.setProperty(ConfigProperty.NotificationList.key(), _notificationList);
        properties.setProperty(ConfigProperty.NotificationType.key(), _notificationType.toString());
        return properties;
    }

    public String getXsiType() {
		return _xsiType;
	}

	public void setXsiType(String _xsiType) {
		this._xsiType = _xsiType;
	}

	public String getProjectId() {
		return _projectId;
	}

	public void setProjectId(String _projectId) {
		this._projectId = _projectId;
	}

    @Column(columnDefinition = "TEXT")
	public String getSchematronContent() {
		return _schematronContent;
	}

	public void setSchematronContent(String _schematronContent) {
		this._schematronContent = _schematronContent;
	}

	public String getSchematronDescription() {
		return _schematronDescription;
	}

	public void setSchematronDescription(String _schematronDescription) {
		this._schematronDescription = _schematronDescription;
	}

	public String getNotificationList() {
		return _notificationList;
	}

	public void setNotificationList(String _notificationList) {
		this._notificationList = _notificationList;
	}

    @Enumerated(EnumType.STRING)
	public NotificationType getNotificationType() {
		return _notificationType;
	}

	public void setNotificationType(NotificationType _notificationType) {
		this._notificationType = _notificationType;
	}
    
}

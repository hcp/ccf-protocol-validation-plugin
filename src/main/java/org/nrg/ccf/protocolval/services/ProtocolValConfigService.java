package org.nrg.ccf.protocolval.services;

import java.util.List;

import org.nrg.ccf.protocolval.daos.ProtocolValConfigDAO;
import org.nrg.ccf.protocolval.entities.ProtocolValConfig;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProtocolValConfigService extends AbstractHibernateEntityService<ProtocolValConfig, ProtocolValConfigDAO> implements BaseHibernateService<ProtocolValConfig>  {

	@Transactional
	public List<ProtocolValConfig> getConfigEntities(String projectId) {
		return getDao().getConfigEntities(projectId);
	}

	@Transactional
	public ProtocolValConfig getConfigEntity(String projectId, String xsiType) {
		return getDao().getConfigEntity(projectId, xsiType);
	}

	@Transactional
	public void saveOrUpdate(ProtocolValConfig config) {
		ProtocolValConfig updateConfig = getDao().getConfigEntity(config.getProjectId(), config.getXsiType());
		if (updateConfig == null) {
			updateConfig = config;
		} else {
			updateConfig.setNotificationList(config.getNotificationList());
			updateConfig.setNotificationType(config.getNotificationType());
			updateConfig.setSchematronDescription(config.getSchematronDescription());
			updateConfig.setSchematronContent(config.getSchematronContent());
		}
		getDao().saveOrUpdate(updateConfig);
	}

	@Transactional
	public void removeConfig(ProtocolValConfig config) {
		ProtocolValConfig removeConfig = getDao().getConfigEntity(config.getProjectId(), config.getXsiType());
		if (removeConfig != null) {
			getDao().delete(removeConfig);
		}
		
	}

}
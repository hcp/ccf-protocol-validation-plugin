package org.nrg.ccf.protocolval.queries;

public class ProtocolValidationQueries {
	
	final static public String WHERE_REPLACE = "$$$WHERE$$$"; 
	
	final static public String FAILED_SESSION_VALIDATION_INFO_QUERY = 
			"SELECT  exp.id as id, exp.project as project, exp.label as label, imexp.id as imageSessionId, imexp.label as imageSessionLabel,  " +
			"        imsess.scanner as imageSessionScanner, imsess.operator as imageSessionOperator, imexp.date as imageSessionDate, " +
			"        val.check_status as overallStatus,  " +
			"        sval.status as status, sval.id as checkId, sval.diagnosis as diagnosis " +
			" FROM " +
			"     xnat_experimentdata exp " +
			"     INNER JOIN " +
			"     xnat_imageassessordata ia " +
			"     ON exp.id = ia.id " +
			"     LEFT JOIN " +
			"     xnat_experimentdata imexp " +
			"     on imexp.id = ia.imagesession_id " +
			"     INNER JOIN " +
			"     xnat_imagesessiondata imsess " +
			"     on imexp.id = imsess.id " +
			"     INNER JOIN " +
			"     val_protocoldata val " +
			"     ON exp.id = val.id " +
			"     INNER JOIN " +
			"     val_protocoldata_condition sval " +
			"     ON val.id = sval.check_conditions_condition_val__id " +
			"	" + WHERE_REPLACE + " " +
			"       and sval.status = 'fail' " +
			"ORDER BY label " +
			"; " 
	;
	
	final static public String FAILED_SCAN_VALIDATION_INFO_QUERY = 
			"SELECT  exp.id as id, exp.project as project, exp.label as label, imexp.id as imageSessionId, imexp.label as imageSessionLabel,  " +
			"        imsess.scanner as imageSessionScanner, imsess.operator as imageSessionOperator, imexp.date as imageSessionDate, " +
			"	     val.check_status as overallStatus,  " +
			"        CASE WHEN sck.scan_id ~ '^[0-9]+$' THEN to_number(sck.scan_id,'99999') ELSE NULL END AS numScanId, " +
			"        sck.scan_id as scanId,  " +
			"        isc.series_description as seriesDescription, isc.type as scanType, isc.quality as scanQuality,   " +
			"        sck.status as scanStatus, scks.id as checkId, scks.status as status, scks.xmlpath as xmlPath, scks.verified as verified, scks.diagnosis as diagnosis " +
			" FROM " +
			"     xnat_experimentdata exp " +
			"     INNER JOIN " +
			"     val_protocoldata val " +
			"     ON exp.id = val.id " +
			"     INNER JOIN " +
			"     xnat_imageassessordata ia " +
			"     ON exp.id = ia.id " +
			"     LEFT JOIN " +
			"     xnat_experimentdata imexp " +
			"     on imexp.id = ia.imagesession_id " +
			"     INNER JOIN " +
			"     xnat_imagesessiondata imsess " +
			"     on imexp.id = imsess.id   " +
			"     INNER JOIN " +
			"     val_protocoldata_scan_check sck " +
			"     ON val.id = sck.scans_scan_check_val_protocolda_id " +
			"     LEFT JOIN " +
			"     val_protocoldata_scan_check_condition scks " +
			"     on sck.val_protocoldata_scan_check_id = scks.conditions_condition_val_protoc_val_protocoldata_scan_check_id  " +
			"     INNER JOIN " +
			"     xnat_imagescandata isc " +
			"     on ia.imagesession_id = isc.image_session_id and isc.id=sck.scan_id " +
			"	" + WHERE_REPLACE + " " +
			"     and scks.status = 'fail'  " +
			"ORDER BY label,numScanId,scanId " +
			"; "
	;
	
}

package org.nrg.ccf.protocolval.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.LifespanUtils;
import org.nrg.ccf.protocolval.components.FailedScanValidationInfo;
import org.nrg.ccf.protocolval.components.FailedSessionValidationInfo;
import org.nrg.ccf.protocolval.components.ValidationInfo;
import org.nrg.ccf.protocolval.queries.ProtocolValidationQueries;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class ProtocolValidationReportUtils {
	
    static final Comparator<Map<String, String>> COMPARATOR = new Comparator<Map<String, String>>() {

			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				int i = o1.get("label").compareTo(o2.get("label"));
				if (i != 0) return i;
				i = o1.get("scanID").compareTo(o2.get("scanID"));
				if (i != 0) return i;
				i = o1.get("checkID").compareTo(o2.get("checkID"));
				return i;		
			}
      		
    };

	public static List<Map<String, String>> getFailureReportTable(String projectId, UserI sessionUser, JdbcTemplate jdbcTemplate) {
		
		final List<Map<String, String>> returnList = new ArrayList<>();
		final List<FailedSessionValidationInfo> sessionValidationInfoList = getSessionValidationInfo(projectId, sessionUser, jdbcTemplate);
		final List<FailedScanValidationInfo> scanValidationInfoList = getScanValidationInfo(projectId, sessionUser, jdbcTemplate);
		for (final FailedSessionValidationInfo sessionCheck : sessionValidationInfoList) {
			// Commented out as this is part of the query
			//if (!sessionCheck.getStatus().toUpperCase().contains("FAIL")) {
			//	continue;
			//}
			Map<String,String> failureMap = newFailureMap(sessionCheck);
			failureMap.put("scanID", "");
			failureMap.put("scanQuality", "");
			failureMap.put("scanType", "");
			failureMap.put("seriesDescription", "");
			failureMap.put("checkID", nullSafe(sessionCheck.getId()));
			failureMap.put("checkDiagnosis", nullSafe(sessionCheck.getDiagnosis()));
			returnList.add(failureMap);
		}
		for (final FailedScanValidationInfo scanCheck : scanValidationInfoList) {
			// Commented out as this is part of the query
			//if (!scanCheck.getStatus().toUpperCase().contains("FAIL")) {
			//	continue;
			//}
			Map<String,String> failureMap = newFailureMap(scanCheck);
			failureMap.put("scanID", nullSafe(scanCheck.getScanId()));
			failureMap.put("scanQuality", nullSafe(scanCheck.getScanQuality()));
			failureMap.put("scanType", nullSafe(scanCheck.getScanType()));
			failureMap.put("seriesDescription", nullSafe(scanCheck.getSeriesDescription()));
			failureMap.put("checkID", nullSafe(scanCheck.getCheckId()));
			failureMap.put("checkDiagnosis", nullSafe(scanCheck.getDiagnosis()));
			returnList.add(failureMap);
		}
	
		Collections.sort(returnList,COMPARATOR);
      		
      	return returnList;
      	
	}

	public static String getFailureReportCSV(String projectId, UserI sessionUser, JdbcTemplate jdbcTemplate) {
		
		final List<Map<String,String>> reportList = getFailureReportTable(projectId, sessionUser, jdbcTemplate); 
		if (reportList.size()<1) {
			return "";
		}
        final SortedSet<String> keys = new TreeSet<>(reportList.get(0).keySet());
        final StringBuffer sb = new StringBuffer();
        sb.append(StringUtils.join(keys, ","));
        sb.append(System.lineSeparator());
        for (final Map<String,String> map : reportList) {
        	final Iterator<String> i = keys.iterator(); 
        	while (i.hasNext()) {
        		final String key = i.next();
        		String val = map.get(key);
        		val = (!val.contains(",")) ? val : "\"" + val.replaceAll("\"", "\\\\\"") + "\"";
        		sb.append(val);
        		if (i.hasNext()) {
        			sb.append(",");
        		}
        		
        	}
       		sb.append(System.lineSeparator());
        }
        return sb.toString();
        
	}

	private static Map<String, String> newFailureMap(ValidationInfo vinfo) {
		
		final Map<String,String> map = new HashMap<>();
		map.put("ID", nullSafe(vinfo.getId()));
		map.put("label", nullSafe(vinfo.getLabel()));
		map.put("imageSessionID", nullSafe(vinfo.getImageSessionId()));
		map.put("imageSessionLabel", nullSafe(vinfo.getImageSessionLabel()));
		map.put("imageSessionScanner", nullSafe(vinfo.getImageSessionScanner()));
		map.put("imageSessionSite", nullSafe(LifespanUtils.getLifespanSiteFromScannerInfo(vinfo.getImageSessionScanner())));
		map.put("imageSessionOperator", nullSafe(vinfo.getImageSessionOperator()));
		map.put("imageSessionDate", nullSafe((vinfo.getImageSessionDate() != null) ?
					CommonConstants.getFormatYYMMDD10().format(vinfo.getImageSessionDate()) : ""));
		return map;
	}
	
	
	private static List<FailedScanValidationInfo> getScanValidationInfo(final String projectId, final UserI sessionUser,
			final JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE exp.project='" + projectId + "'";
		final String queryStr = ProtocolValidationQueries.FAILED_SCAN_VALIDATION_INFO_QUERY.replace(ProtocolValidationQueries.WHERE_REPLACE, whereStr);
		final List<FailedScanValidationInfo> scanValidations = jdbcTemplate.query(queryStr,
				new BeanPropertyRowMapper<FailedScanValidationInfo>(FailedScanValidationInfo.class));
		return scanValidations;
	}

	private static List<FailedSessionValidationInfo> getSessionValidationInfo(final String projectId,final UserI sessionUser,
			final JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE exp.project='" + projectId + "'";
		final String queryStr = ProtocolValidationQueries.FAILED_SESSION_VALIDATION_INFO_QUERY.replace(ProtocolValidationQueries.WHERE_REPLACE, whereStr);
		final List<FailedSessionValidationInfo> sessionValidations = jdbcTemplate.query(queryStr,
				new BeanPropertyRowMapper<FailedSessionValidationInfo>(FailedSessionValidationInfo.class));
		return sessionValidations;
		
	}
	
	private static String nullSafe(String ins) {
		return (ins != null) ? ins : "";
	}

}

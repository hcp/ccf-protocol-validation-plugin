package org.nrg.ccf.protocolval.utils;

import java.io.StringReader;

import org.nrg.xdat.om.base.BaseXnatExperimentdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.xml.sax.InputSource;

public class ValidateUtils {

	public static String storeAssessorXml(String validationXml, String validationLabel, UserI user) throws Exception {
		
		final StringReader sr = new StringReader(validationXml);
		final InputSource is = new InputSource(sr);
		final SAXReader reader = new SAXReader(user);
		final XFTItem item = reader.parse(is);
		final String id = BaseXnatExperimentdata.CreateNewID();
		item.setProperty("ID",id);
		item.setProperty("label",validationLabel);
		SaveItemHelper.unauthorizedSave(item, user, false, true,EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
		return id;
	}

}

package org.nrg.ccf.protocolval.util;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMSource;
import org.apache.log4j.Logger;
import java.io.InputStream;
import java.util.List;
import org.nrg.ccf.protocolval.util.PluginUriResolver;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;

public class PluginUriResolver implements URIResolver {
	
	private DocumentBuilder _builder;
	private final static Logger _logger = Logger.getLogger(PluginUriResolver.class); 
	
	public PluginUriResolver(DocumentBuilder builder) {
		super();
		_builder = builder;
	}

	@Override
	public Source resolve(String href, String base) throws TransformerException {
		
		try {
			_logger.error("href=" + href);
			_logger.error("base=" + base);
			final List<Resource> resList = BasicXnatResourceLocator.getResources("/META-INF/protocolVal/*");
			resList.addAll(BasicXnatResourceLocator.getResources("/META-INF/protocolVal/*/*"));
			for (final Resource res : resList) {
				if (res.getFilename().equals(href)) {
					final InputStream stream = res.getInputStream();
					Document doc = _builder.parse(stream);
					Source src = new DOMSource(doc);
					stream.close();
					return src;
				}
			}
			return null;
		} catch (Exception e) {
			return null;
		}	
		
	}

}

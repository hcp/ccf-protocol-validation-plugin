package org.nrg.ccf.protocolval.constants;

public enum NotificationType {
	
	NONE, ALWAYS, ON_FAIL

}

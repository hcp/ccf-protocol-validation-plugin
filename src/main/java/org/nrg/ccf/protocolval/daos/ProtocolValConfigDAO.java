package org.nrg.ccf.protocolval.daos;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.ccf.protocolval.entities.ProtocolValConfig;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class ProtocolValConfigDAO extends AbstractHibernateDAO<ProtocolValConfig> {

	@SuppressWarnings("unchecked")
	public List<ProtocolValConfig> getConfigEntities(String projectId) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public ProtocolValConfig getConfigEntity(String projectId, String xsiType) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		criteria.add(Restrictions.eq("xsiType",xsiType));
		return (ProtocolValConfig)criteria.uniqueResult();
	}

}

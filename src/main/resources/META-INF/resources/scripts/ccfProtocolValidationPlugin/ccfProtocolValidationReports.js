
//# sourceURL=ccrProtocolValidationPlugin/ccfProtocolValidationReports.js

if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.protocolvalreport === 'undefined') {
	CCF.protocolvalreport = { };
}


if (typeof CCF.protocolvalreport.project === 'undefined' || CCF.protocolvalreport.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.protocolvalreport.project = queryParams.get("project");
}

CCF.protocolvalreport.initFailureReportTable = function() {

	$("a[href='#val-failure-tab']").click(function() {
		setTimeout(function(){
			CCF.protocolvalreport.populateRowCount();
		},10);
	});

  $('#valReport-contents').html("<div id='val-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='valFailureReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='valreports-row-count-container''></div>");

    var columnVar = 
           {
              assessor: {
                label: 'Assessor',
                filter: true,
            	td: {'className': 'assessor center rowcount'},
                apply: function() {
                  return "<a href='/data/experiments/" + this.ID + "?format=html' target='_blank'>" + this.label.replace(/_PC_.*$/,"") + "</a>";
                },
                sort: true
              },
              imageSessionDate: {
                label: 'sessionDate',
                filter: true,
            	td: {'className': 'sessionDate center'},
                sort: true
              },
              imageSessionSite: {
                label: 'Site',
                filter: true,
            	td: {'className': 'site center'},
                sort: true
              },
              imageSessionScanner: {
                label: 'Scanner',
                filter: true,
            	td: {'className': 'scanner center'},
                sort: true
              },
              imageSessionOperator: {
                label: 'Operator',
                filter: true,
            	td: {'className': 'operator center'},
                sort: true
              },
              scanID: {
                label: 'ScanId',
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'ALL', value: 'all' },
                       { label: 'EMPTY', value: 'empty' },
                       { label: 'NOT EMPTY', value: 'not_empty' }
                     ],
                     element: filterMenuElement.call(table, 'scanID')
                   }).element])
                 },
            	td: {'className': 'scanID center'},
                sort: true
              },
              scanQuality: {
                label: 'Quality',
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'ALL', value: 'all' },
                       { label: 'NOT UNUSABLE', value: 'not_unusable' },
                       { label: 'UNUSABLE', value: 'unusable' }
                     ],
                     element: filterMenuElement.call(table, 'scanQuality')
                   }).element])
                 },
            	td: {'className': 'scanQuality center'},
                sort: true
              },
              scanType: {
                label: 'ScanType',
                filter: true,
            	td: {'className': 'scanType center'},
                sort: true
              },
              seriesDescription: {
                label: 'SeriesDesc',
                filter: true,
            	td: {'className': 'seriesDescription center'},
                sort: true
              },
              checkID: {
                label: 'CheckID',
                filter: true,
            	td: {'className': 'checkID center', 'style': 'min-width:210px;max-width:315px;'},
                sort: true
              },
              checkDiagnosis: {
                label: 'Diagnosis',
                filter: true,
            	td: {'className': 'checkDiagnosis center', 'style': 'min-width:280px;max-width:560px;'},
                sort: true
              }

           }
       ;

    // set up custom filter menus
    function filterMenuElement(prop){
      //console.log(prop);
      if (!prop) return false;
      // call this function in context of the table
      var $pipelineTable = $(this);
      //console.log($pipelineTable);
      var FILTERCLASS = 'filter-' + prop;
      //console.log(FILTERCLASS);
      return {
        id: 'pcp-filter-select-' + prop,
        on: {
          change: function() {
            var selectedValue = $(this).val();
            $("td." + prop).each(function() {
              if ($(this).hasClass("filter")) {
                  return true;
              }
              var $row = $(this).closest('tr');
               //console.log($row)
              if (selectedValue === 'all') {
                $row.removeClass(FILTERCLASS);
                return;
              }
    
              $row.addClass(FILTERCLASS);
              if (selectedValue === 'empty') {
		if (this.textContent == "") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (selectedValue === 'not_empty') {
		if (this.textContent != "") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (selectedValue === 'unusable') {
		if (this.textContent == "unusable") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (selectedValue === 'not_unusable') {
		if (this.textContent != "unusable") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (this.textContent.includes(selectedValue)) {
                $row.removeClass(FILTERCLASS);
              }
            })
    
    	CCF.protocolvalreport.populateRowCount();
    
          }
        }
      };
    }


 setTimeout(function(){
        $("li[data-tab='qc-summary-tab']").removeClass("active");
        $("#protocolValGroup").find("li").removeClass("active");
        $("li[data-tab='qc-summary-tab']").addClass("active");
  },100)
  XNAT.xhr.getJSON({
     url: '/xapi/protocolValidationReports/project/' + CCF.protocolvalreport.project +
          '/failureReport',
     success: function(data) {
	CCF.protocolvalreport.failureData = data;
        XNAT.table.dataTable(data, {
           table: {
              id: 'val-qc-datatable',
              className: 'val-qc-table highlight'
           },
           width: 'fit-content',
           overflowY: 'hidden',
           overflowX: 'hidden',
           columns: columnVar
        }).render($('#valFailureReport-table'))
        $("#valFailureReport-table").find(".data-table-wrapper").css("display","table");
     	$("#val-building-div").html("<div style='width:100%;text-align:right'><a id='val-download-csv' download='ProtocolValFailureReport.csv' onclick='CCF.protocolvalreport.doDownload()' type='text/csv'>Download CSV</a></div>");
        setTimeout(function(){
        	CCF.protocolvalreport.populateRowCount();
        },100);
        $('input.filter-data').keyup(function() {
            setTimeout(function(){
               CCF.protocolvalreport.populateRowCount();
           },50)
        });
/*
        setTimeout(function(){
    		$('.highlight').on("click",function() {
			$(this).closest('tr').find('td').each(function(){
				if ($(this).css("background-color").toString().indexOf('238')>=0) {
					$(this).css("background-color","");
				} else {
					$(this).css("background-color","rgb(238,238,238)");
				}
			});
		});
        },100);
*/
     },
     failure: function(data) {
     	$("#val-building-div").html("<h3>Failed to build report</h3>");
     }
  });

}

CCF.protocolvalreport.doDownload = function() {
	var csv = CCF.protocolvalreport.generateCSV();
	var data = new Blob([csv]);
	var ele = document.getElementById("val-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.protocolvalreport.generateCSV = function() {

    var dta = CCF.protocolvalreport.failureData;
    var result = "Session,SessionDate,Site,Scanner,Operator,ScanID,Quality,ScanType,SeriesDesc,CheckID,Diagnosis\n";
    dta.forEach(function(obj){
	console.log(obj);
        var checkDiagnosis = obj["checkDiagnosis"].replace(/\r?\n|\r/g,"");
        checkDiagnosis = (checkDiagnosis.indexOf(",")<0) ? checkDiagnosis : "\"" + checkDiagnosis.replace('"','\\"') + "\"";
        result += ((obj["imageSessionLabel"].indexOf(",")<0) ? obj["imageSessionLabel"] : "\"" + obj["imageSessionLabel"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["imageSessionDate"].indexOf(",")<0) ? obj["imageSessionDate"] : "\"" + obj["imageSessionDate"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["imageSessionSite"].indexOf(",")<0) ? obj["imageSessionSite"] : "\"" + obj["imageSessionSite"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["imageSessionScanner"].indexOf(",")<0) ? obj["imageSessionScanner"] : "\"" + obj["imageSessionScanner"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["imageSessionOperator"].indexOf(",")<0) ? obj["imageSessionOperator"] : "\"" + obj["imageSessionOperator"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["scanID"].indexOf(",")<0) ? obj["scanID"] : "\"" + obj["scanID"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["scanQuality"].indexOf(",")<0) ? obj["scanQuality"] : "\"" + obj["scanQuality"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["scanType"].indexOf(",")<0) ? obj["scanType"] : "\"" + obj["scanType"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["seriesDescription"].indexOf(",")<0) ? obj["seriesDescription"] : "\"" + obj["seriesDescription"].replace('"','\\"') + "\"");
        result += ",";
        result += ((obj["checkID"].indexOf(",")<0) ? obj["checkID"] : "\"" + obj["checkID"].replace('"','\\"') + "\"");
        result += ",";
        result += checkDiagnosis;
        result += "\n";
    });

    //var keys = Object.keys(dta[0]);

    //var result = keys.join(",") + "\n";

    //dta.forEach(function(obj){
    //    keys.forEach(function(k, ix){
    //        if (ix) result += ",";
    //        result += ((obj[k].indexOf(",")<0) ? obj[k] : "\"" + obj[k].replace('"','\\"') + "\"");
    //    });
    //    result += "\n";
    //});

    return result;

}


CCF.protocolvalreport.populateRowCount = function() {
   var rowCount = ($('.rowcount').closest('tr').filter(":visible")).length;
    if ($("#val-building-div").find('img').length<1) {
    	$('#valreports-row-count-container').html("<em>" + rowCount + " rows match query filters.</em>");
    } else {
    	$('#valreports-row-count-container').empty();
    }
}



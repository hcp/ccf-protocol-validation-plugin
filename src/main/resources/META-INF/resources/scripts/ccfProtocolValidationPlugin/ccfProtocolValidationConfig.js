
if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.pvalconfig === 'undefined') {
	CCF.pvalconfig = {
				prevData: [],
				currentTag: "",
				currentProject: ""
			 };
}


CCF.pvalconfig.queryParams = new URLSearchParams(window.location.search);
CCF.pvalconfig.currentProject = CCF.pvalconfig.queryParams.get("id");
//$("#create-new-pvalconfig").prop('disabled',true);
$("#create-new-pvalconfig").hide();
$("#protocol-validation-list-container").hide();
$("#protocol-validation-building-container").html("<div id='building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Retrieving Configuration.</div>");
$("#protocol-validation-building-container").show();
XNAT.xhr.getJSON({
	 	url: '/xapi/projects/' + CCF.pvalconfig.currentProject + '/getImageSessionTypes',
	 	success: function(data) {
	 	CCF.pvalconfig.sessionTypes = data;
	 	//$("#create-new-pvalconfig").prop('disabled',false);
		$("#create-new-pvalconfig").show();
		$("#protocol-validation-list-container").show();
		$("#protocol-validation-building-container").hide();
	 }
});

CCF.pvalconfig.populateSettingsTable = function(tag) {

	var columnVar = {
		viewEdit: {
			label: ' ',
			filter: false,
			td: {'className': 'viewEditConfig center'},
			sort: false,
			apply: function() {
				return "<a href=\"javascript:CCF.pvalconfig.editConfig('" + this.xsiType + "');\"> View/Edit</a>";
			}
		},
		xsiType: {
			label: 'xsiType',
			filter: false,
			td: {'className': 'xsiType center'},
			sort: false
		},
		schematronDescription: {
			label: 'Schematron',
			filter: false,
			td: {'className': 'schematronDescription center'},
			sort: false
		},
		remove: {
			label: ' ',
			filter: false,
			td: {'className': 'removeconfig center'},
			sort: false,
			apply: function() {
				return "<button onclick=\"CCF.pvalconfig.removeConfig('" + this.xsiType + "');\"> Delete</button>";
			}
		}
	};

	XNAT.xhr.getJSON({
		url: '/xapi/projects/' + CCF.pvalconfig.currentProject + '/protocolValidationConfig',
		success: function(data) {

			CCF.pvalconfig.currentData = data;

			if (data.length < 1) {
				$(tag).html("<h3>Protocol validation has not been set up for this project.</h3>");
				return;
			}

			$(tag).html("");
			XNAT.table.dataTable(CCF.pvalconfig.currentData, {
				table: {
					id: 'ccf-pvalconfig',
					className: 'ccf-pvalconfig-table'
				},
				width: 'fit-content',
				overflowY: 'hidden',
				overflowX: 'hidden',
				columns: columnVar
			}).render($(tag))
			$(tag).find(".data-table-wrapper").css("display","table");
		},
		failure: function(data) {
			CCF.pvalconfig.currentData = [];
			$(tag).html("<h3>Protocol validation has not been set up for this project.</h3>");
		}
	});

}

CCF.pvalconfig.editConfig = function(xsiType) {

	for (var i=0; i<CCF.pvalconfig.currentData.length; i++) {
		var currentRec = CCF.pvalconfig.currentData[i];
		if (currentRec.xsiType == xsiType && currentRec.projectId == CCF.pvalconfig.currentProject) {
			return CCF.pvalconfig.editValidationType(currentRec);
		}
	}
	xmodal.message("Error","Could not find requested record");

}

CCF.pvalconfig.removeConfig = function(xsiType) {

	var toBeRemoved;
	for (var i=0; i<CCF.pvalconfig.currentData.length; i++) {
		var currentRec = CCF.pvalconfig.currentData[i];
		if (currentRec.xsiType == xsiType && currentRec.projectId == CCF.pvalconfig.currentProject) {
			toBeRemoved = currentRec;
		}
	}
	if (typeof toBeRemoved === 'undefined') {
		xmodal.message("Error","Could not find requested record");
	}

	xmodal.confirm({
		title: 'Delete Validation Configuration?',
		content: 'Delete Valdation Configuration: ' + '<b>' + toBeRemoved.xsiType + '</b>',
		okLabel: 'Delete',
		okClose: false,
		okAction: function(dlg){
			CCF.pvalconfig.removeConfigRecord(toBeRemoved);
		}
	});

}

CCF.pvalconfig.newValidationType = function() {

	CCF.pvalconfig.editValidationType();

}

CCF.pvalconfig.removeConfigRecord = function(configEle) {

	XNAT.xhr.delete({
		url: '/xapi/projects/' + CCF.pvalconfig.currentProject + '/protocolValidationConfig',
		cache: false,
		async: true,
		contentType: 'application/json; charset=utf-8',
		//context: this,
		data:  JSON.stringify(configEle),
		dataType: 'json'
	}).done(function(data) {
	  	$(".top-banner").hide();
		xmodal.closeAll();
		XNAT.ui.banner.top(3000, 'Configuration successfully deleted.', 'success');
	}).fail(function(data, textStatus, jqXHR) {
		xmodal.closeAll();
		xmodal.message("Error","ERROR:  Could not delete the configuration. (STATUS=" + textStatus + ").");
	}).always( function(data) {
		CCF.pvalconfig.populateSettingsTable('#protocol-validation-list-container');
	});

}

CCF.pvalconfig.saveConfigRecord = function(configEle) {

	XNAT.xhr.post({
		url: '/xapi/projects/' + CCF.pvalconfig.currentProject + '/protocolValidationConfig',
		cache: false,
		async: true,
		contentType: 'application/json; charset=utf-8',
		//context: this,
		data:  JSON.stringify(configEle),
		dataType: 'json'
	}).done(function(data) {
	  	$(".top-banner").hide();
		XNAT.ui.banner.top(3000, 'Configuration successfully saved.', 'success');
	}).fail(function(data, textStatus, jqXHR) {
		xmodal.message("Error","ERROR:  Could not save the configuration. (STATUS=" + textStatus + ").");
	}).always( function(data) {
		CCF.pvalconfig.populateSettingsTable('#protocol-validation-list-container');
	});

}

CCF.pvalconfig.editValidationType = function(currentRec) {

	if (typeof CCF.pvalconfig.sessionTypes === 'undefined') {
		return;
	}

	var sessionTypes = Array.from(CCF.pvalconfig.sessionTypes);

	if (typeof currentRec === 'undefined') {
	
		$.each(CCF.pvalconfig.currentData, function(inx, val) {
			var type = val.xsiType;
			for (var i=0; i<sessionTypes.length; i++) {
				if (type==sessionTypes[i]) {
					sessionTypes.splice(i,1);
				}
			}
		});
	
		var typeOptions = {};
		for (var i=0; i<sessionTypes.length; i++) {
			var typeVal = sessionTypes[i];
			typeOptions[typeVal] = typeVal;
		}

		if (sessionTypes.length<1) {
			xmodal.message("Error","There are no image session types for this project that have not been set up for protocol validation.");
			return;
		}

	} else {

		var typeOptions = {};
		typeOptions[currentRec.xsiType] = currentRec.xsiType;

	}

	xmodal.open({
		title: 'New Protocol Validation Configuration',
		id: "view-modal",
		width: 880,
		height: 725,
		content: '<div id="protocolValConfig"></div>',
		okLabel: 'Submit',
		okClose: false,
		cancel: 'Cancel',
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.pvalconfig.populateSettingsTable('#protocol-validation-list-container');
			xmodal.closeAll();
		}),
		okAction: (function() {
			var xsiType = $("#input-xsiType").val();
			var notificationType = $("#input-notificationType").val();
			var notificationList = $("#input-notificationList").val();
			var schematronDescription = $("#input-schematronDescription").val();
			var schematronContent = CCF.pvalconfig.aceEditor.getValue();
			var configOk = (!(typeof xsiType === 'undefined' || xsiType.trim().length<1 ||
			    typeof notificationType === 'undefined' || notificationType.trim().length<1 ||
			    typeof schematronDescription === 'undefined' || schematronDescription.trim().length<1 ||
			    typeof schematronContent === 'undefined' || schematronContent.trim().length<1));
			var newConfig = {};
			newConfig["projectId"]=CCF.pvalconfig.currentProject;
			newConfig["xsiType"]=xsiType;
			newConfig["notificationType"]=notificationType;
			newConfig["notificationList"]=notificationList;
			newConfig["schematronDescription"]=schematronDescription;
			newConfig["schematronContent"]=schematronContent;
			if (configOk == true) {
				xmodal.closeAll();
				CCF.pvalconfig.saveConfigRecord(newConfig);
			} else {
				xmodal.message("You must provide values for all fields other than the notification list.");
			}
		})
	});

	var notifyOptions = {
		"ALWAYS": "Always",
		"ON_FAIL": "On Fail",
		"NONE": "Never"
	}

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				xsiType: {
					kind: 'panel.select.single',
					id: 'input-xsiType',
					name: 'input-xsiType',
					className: 'required srconfig-ele',
					label: 'XSIType',
					options: typeOptions
				},
				notificationType: {
					kind: 'panel.select.single',
					id: 'input-notificationType',
					name: 'input-notificationType',
					className: 'required srconfig-ele',
					label: 'When to Notify:',
					options: notifyOptions
				},
				notificationList: {
					kind: 'panel.input.text',
					id: 'input-notificationList',
					name: 'input-notificationList',
					className: 'srconfig-ele',
					size: '70',
					label: 'Notification List:',
					description: 'The notification list should be a comma-separated list of e-mail addresses.'
					//value: reportInfo.description
				},
				schematronDescription: {
					kind: 'panel.input.text',
					id: 'input-schematronDescription',
					name: 'input-schematronDescription',
					className: 'required srconfig-ele',
					size: '50',
					label: 'Schematron Description:'
					//value: reportInfo.description
				},
				schematronElement: {
					kind: 'panel.element',
					id: 'input-schematronElement',
					name: 'input-schematronElement',
					label: 'Schematron Content:',
					contents: "<em>Enter schematron content in the editor box below.</em>" 
				}
			}
		}
	};

	XNAT.spawner.spawn(viewConfig).render($("#protocolValConfig")[0]);
        $body.unbind('keydown');
	$("#protocolValConfig").append(
            '<br><div class="editor-wrapper" style="width:840px;height:320px;position:relative;">' +
            '    <div id="schematron-content" name="schematron-content" class="editor-content" style="position:absolute;top:0;right:0;bottom:0;left:0;border:1px solid #ccc;"></div>' +
            '</div>');
	setTimeout(function() {
		CCF.pvalconfig.aceEditor = ace.edit("schematron-content");
		CCF.pvalconfig.aceEditor.setTheme("ace/theme/eclipse");
		CCF.pvalconfig.aceEditor.session.setMode("ace/mode/xml");
		if (typeof currentRec === 'undefined') {
			CCF.pvalconfig.aceEditor.setValue("");
			$("#input-notificationType").val("ON_FAIL");
			if (sessionTypes.length == 1) {
				$("#input-xsiType").val(sessionTypes[0]);
			}
		} else {
			$("#input-xsiType").val(currentRec.xsiType);
			$("#input-notificationType").val(currentRec.notificationType);
			$("#input-notificationList").val(currentRec.notificationList);
			$("#input-schematronDescription").val(currentRec.schematronDescription);
			CCF.pvalconfig.aceEditor.setValue(currentRec.schematronContent);
			$("#input-xsiType").prop('disabled',true);
		}
	}, 100);


}




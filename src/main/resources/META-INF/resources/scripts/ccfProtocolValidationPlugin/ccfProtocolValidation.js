
if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.protocolval === 'undefined') {
	CCF.protocolval = {
			 };
}

CCF.protocolval.initialize = function() {

	console.log("Initializing protocol validation");
	XNAT.xhr.getJSON({
		url: '/xapi/projects/' + XNAT.data.context.projectID + '/protocolValidationConfig?condensed=true',
		success: function(data) {
			if (data.length < 1) {
				return;
			}
			for (var i=0; i<data.length; i++) {
				if (XNAT.data.context.xsiType==data[i]["xsiType"]) {
					CCF.protocolval.defaultNotificationType = data[i]["notificationType"];
					// NOTE:  Using mousedown instead of click event.  The click event doesn't seem to be reliable for elements
					// dynamically attached to the YUI menu.  It is frequently prevented from running, presumably by earlier attached events.
					$("#Processbox").find("ul").append(
				    		'<li class="yuimenuitem run-protocol-validation-li">' +
				    		'<a class="yuimenuitemlabel protocol-validation-launch" title="Run protocol validation" href="#!"' + 
							'onmousedown="CCF.protocolval.runProtocolValidation()">Run protocol validation</a>' +
				    	'</li>'
					);
					return;
				}
			}
		},
		failure: function(data) {
			console.log("Error returning protocol validation configuration");
		}
	});

}

CCF.protocolval.toggleFailureView = function() {

	if ($(".fieldpass").closest('tr').is(":hidden")) {
		$(".fieldpass").closest('tr').show();
		$(".fieldpass").parent('.scancheckdiv').show();
		$(".fieldpass").parent().parent('.sessioncheckdiv').show();
		$(".fieldNo").closest('tr').show();
		$(".fieldNo").parent('.scancheckdiv').show();
		$(".fieldNo").parent().parent('.sessioncheckdiv').show();
	} else {
		$(".fieldpass").closest('tr').hide();
		$(".fieldpass").parent('.scancheckdiv').hide();
		$(".fieldpass").parent().parent('.sessioncheckdiv').hide();
		$(".fieldno").closest('tr').hide();
		$(".fieldNo").parent('.scancheckdiv').hide();
		$(".fieldNo").parent().parent('.sessioncheckdiv').hide();
	} 

}

CCF.protocolval.initializeFailureView = function() {

	$.ready(function() {
		setTimeout(function() {
			if ($(".fieldfail").length<1) {
				$(".failureViewLink").hide();
			} else {
				$(".failureViewLink").show();
			}
		}, 200);
	});

}

CCF.protocolval.runProtocolValidation = function() {

	console.log("Opening protocol validation launch modal for " + XNAT.data.context.label);

	xmodal.confirm({
		title: "Launch Protocol Validation",
		content: "<h3>Launch protocol validation for " + XNAT.data.context.label + "?</h3><br>" +
			 "<div id='notifyDiv'></div>",
		width: 500,
		height: 250,
		okLabel: "Run Validation",
		okClose: true,
		okAction: function(dlg){

			CCF.protocolval.showAssessor = true;
			xmodal.open({
				title: 'Protocol Validation Running',
				width: 500,
				height: 250,
				content: "<div id='s-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Processing.</div>",
				okLabel: "Don't Wait",
				cancel: false,
				okClose: false,
				okAction: (function() {
					CCF.protocolval.showAssessor = false;
					xmodal.closeAll();
				})
			});

			var projId = XNAT.data.context.projectID;
			var subjId = XNAT.data.context.subjectLabel;
			var expLbl = XNAT.data.context.label;
			var notify = $("#input-notificationType").val();
			if (typeof notify !== 'undefined' && notify.length>0) {
				notify = "?notification=" + notify;
			} else {
				notify = "";
			}

			XNAT.xhr.post({
				url: '/xapi/projects/' + projId + '/subjects/' + subjId + '/experiments/' + expLbl + '/runProtocolValidation' + notify,
				cache: false,
				async: true,
			}).done(function(data) {
			  	$(".top-banner").hide();
				if (data.length > 1 && data.length < 25) {
					if (CCF.protocolval.showAssessor == true) {
						window.location.href = serverRoot + 
							"/app/action/DisplayItemAction/search_element/val:protocolData/search_field/val:protocolData.ID/search_value/" + data;
					} else {
						XNAT.ui.banner.top(3000, 'Protocol validation processing completed.', 'success');
						$(".edit_header1").each(function() {
							if ($(this).text().includes("Assessments")) {
								var thisTable = $(this).next();
								thisTable.find("tr").each(function() {
									var thisTr = this;
									$(this).find("a").each(function() {
										var href = $(this).attr("href");
										if (typeof href !== 'undefined' && href.includes("protocolData")) {
											thisTr.remove();
										}
									});
								});
								var appendStr = "<tr><td><a href='" + serverRoot +
									 "/app/action/DisplayItemAction/search_element/val:protocolData/search_field/val:protocolData.ID/search_value/" +
									 data + "'>Protocol Val</a></td><td>" + new Date().toISOString().slice(0,10) +
									 "</td><td>" + XNAT.data.context.project + "</td></tr>";
								thisTable.append(appendStr);
							}
						});
					}
				} else {
					xmodal.closeAll();
					xmodal.message("Warning","WARNING:  Unexpected output returned from processing request.  Status is undetermined. (OUTPUT=" + data + ").");
				}
			}).fail(function(data, textStatus, jqXHR) {
				xmodal.closeAll();
				xmodal.message("Error","ERROR:  Protocol validation processing failed (STATUS=" + textStatus + ").");
			});

		}
	});
	var notifyOptions = {
		"ALWAYS": "Always",
		"ON_FAIL": "On Fail",
		"NONE": "None"
	}

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				notificationType: {
					kind: 'panel.select.single',
					id: 'input-notificationType',
					name: 'input-notificationType',
					className: 'required srconfig-ele',
					label: 'Notification:',
					options: notifyOptions
				}
			}
		}
	};
	XNAT.spawner.spawn(viewConfig).render($("#notifyDiv")[0]);
	setTimeout(function() {
		$("#input-notificationType").val(CCF.protocolval.defaultNotificationType);
	}, 100);
}


